#include <fstream>
#include "managerdao_file_impl.h"
#include "emis.h"
void ManagerDaoFileImpl::load(vector<Manager>& managers)
{
	managers.clear();
    chdir("data");
    fstream fin("managers.dat",ios::in|ios::binary);
    //检查打开失败
    char bufdept[MAX_STRING];
    while(fin.getline(bufdept,MAX_STRING))
    {
        fin.seekg(1,ios::cur);//跳过\n
        int id=0;
        char name[20]="";
        char password[20]="";
        sscanf(bufdept,"%d %s %s",&id,name,password);
        Manager mage(id,name,password);
        managers.push_back(mage);
    }
    fin.close();
}
void ManagerDaoFileImpl::save(vector<Manager>& managers)
{
    chdir("data");
    fstream fout("managers.dat",ios::out|ios::binary);
//    检查打开失败
    for(int i=0;i<m_vecMages.size();i++)
    {
        char bufdept[MAX_STRING];
		int id = managers[i].get_id();
		char name[20];
		strcpy(name,managers[i].get_name().c_str());
        char password[20];
        strcpy(password,managers[i].get_password().c_str());

        sprintf(bufdept,"%d %s %s\n",id,name,password);

        fout.write(bufdept,strlen(bufdept)+1); 
    }
    fout.close();
}
