#include "managerservice.h"
class ManagerServiceImpl:public ManagerService
{
public:
	bool addManager(Manager manager);
	bool deleteManager(int id);
	void listManager(void);
};