#include<stdio.h>
#include<getch.h>
#include "serviceview_console_impl.h"//类声明文件
#include "servicedao_file_impl.h"//文件存储类文件
#include "service_impl.h"//服务操作类文件
#include "emis.h"//全局变量
#include "tools.h"
using namespace std;
void ServiceView_impl::menu(void)
{
	ServiceDao_impl dao;//存储
	dao.load(m_vecDepts);
	while(1)
	{
    	cout<<"1、增加部门"<<endl;
		cout<<"2、删除部门"<<endl;
		cout<<"3、列出部门"<<endl;
		cout<<"4、增加员工"<<endl;
		cout<<"5、删除员工"<<endl;
		cout<<"6、修改员工信息"<<endl;
		cout<<"7、列出部门员工"<<endl;
		cout<<"8、列出所有员工"<<endl;
		cout<<"9、保存退出"<<endl; 
		
		switch(get_cmd('1','9'))
		{
			case '1':addDept();break;
			case '2':deleteDept();break;
			case '3':listDept();break;
			case '4':addEmp();break;
			case '5':deleteEmp();break;
			case '6':modifyEmp();break;
			case '7':listEmp();break;
			case '8':listAllEmp();break;
			case '9':dao.save(m_vecDepts);return;
		}
	}
}
void ServiceView_impl::addDept(void)
{
	cout<<"请输入需要增加的部门的ID号:";
	int id;
	cin>>id;

	cout<<"请输入要增加部门的名称:";
	string name;
	cin>>name;

	Department d1(id,name);

	Service_impl s;

	if(!s.addDept(d1))
	{
		cout<<"增加部门失败"<<endl;
	}else
	{
		cout<<"增加部门成功"<<endl;
	}
	
}
void ServiceView_impl::deleteDept(void)
{
	cout<<"请输入你要删除的部门:";
	int deptid;
	cin>>deptid;

	Service_impl s;

	if(!s.deleteDept(deptid))
	{
		cout<<"删除部门失败"<<endl;
	}else
	{
		cout<<"删除部门成功"<<endl;
	}
	
}
void ServiceView_impl::listDept(void)
{
	Service_impl s;
	s.listDept();
}
void ServiceView_impl::addEmp(void)
{
	cout<<"请输入要增加的员工的部门ID号:";
	int deptid;
	cin>>deptid;

	int k=-1;
	for(int i=0;i<m_vecDepts.size();i++)
	{
		if(deptid==m_vecDepts[i].get_d_nId())
		{
			k=i;
			cout<<"要添加的部门已经找到"<<endl;
			break;
		}
	}
	if(k<0)
	{
		cout<<"没有该部门"<<endl;
		return;
	}
	cout<<"请输入要增加的员工的ID号:";
	int id;
	cin>>id;

	cout<<"请输入要增加的员工的姓名:";
	string name;
	cin>>name;

	cout<<"请输入要增加的员工的性别(1=male，0=female):";
	bool gender;
	cin>>gender;

	cout<<"请输入要增加的员工的年龄:";
	int age;
	cin>>age;

	Employee e1(id,name,gender,age);

	Service_impl s;
    if(!s.addEmp(deptid,e1))
	{
		cout<<"增加员工失败"<<endl;
	}else
	{
		cout<<"增加员工成功"<<endl;
	}
}

void ServiceView_impl::deleteEmp(void)
{
	cout<<"请输入要删除的员工的部门ID号：";
	int deptid;
	cin>>deptid;
	cout<<"请输入要删除的员工的ID号：";
	int empid;
	cin>>empid;
	Service_impl s;

	if(!s.deleteEmp(deptid,empid))
	{
		cout<<"删除员工失败"<<endl;
	}else
	{
		cout<<"删除员工成功"<<endl;
	}
	
}

void ServiceView_impl::modifyEmp(void)
{
	cout<<"请输入要修改的员工的部门ID号：";
	int deptid;
	cin>>deptid;

	cout<<"请输入要修改的员工的ID号：";
	int empid;
	cin>>empid;

	Service_impl s;
	if(s.modifyEmp(deptid,empid))
	{
		cout<<"修改员工信息失败"<<endl;
	}else
	{
		cout<<"修改员工信息成功"<<endl;
	}
}
void ServiceView_impl::listEmp(void)
{
	cout<<"请输入你要查询部门的ID:";
	int deptid;
	cin>>deptid;
	
	Service_impl s;
	if(!s.listEmp(deptid))
	{
		cout<<"查询失败部门不存在"<<endl;
	}
	
}
void ServiceView_impl::listAllEmp(void)
{
	Service_impl s;
	s.listAllEmp();
}

