touch ManagerView.cpp				//接口类：ManagerView

作为管理子系统用户界面层的接口类，ManagerView 类被定义为纯抽象类，由 4 个纯虚函数组成。
•menu()：显示主菜单
•add()：处理增加管理员菜单项
•del()：处理删除管理员菜单项
•list()：处理列出所有管理员菜单项

touch ManagerViewConsoleImpl.cpp	//实现类：ManagerViewConsoleImpl

作为管理子系统用户界面层的实现类，ManagerViewConsoleImpl 从纯抽象类 ManagerView 继承，并对基类中的 4 个纯虚函数提供覆盖版本。
•menu()：通过控制台显示主菜单。在一个无限循环中不停显示菜单，接受用户选择，并根据用户所选菜单项调用其它三个接口函数。当用户选择进入运营管理子菜单时，应创建ServiceViewConsoleImpl对象，并调用其menu()接口。当用户选择退出时，终止循环，返回main()函数
•add()：通过控制台处理增加管理员菜单项。根据用户输入的管理员用户名和密码创建Manager对象，调用ManagerService::addManager()接口函数增加管理员
•del()：通过控制台处理删除管理员菜单项。根据用户输入的管理员ID号，调用ManagerService::deleteManager()接口函数删除管理员
•list()：通过控制台处理列出所有管理员菜单项。调用ManagerService::listManager()接口函数获得管理员容器，遍历该容器并列表显示
•ManagerService* service：业务逻辑对象。构造函数中动态创建ManagerServiceImpl对象。

4.1.2. 业务逻辑

touch ManagerService.cpp//接口类：ManagerService

作为管理子系统业务逻辑层的接口类，ManagerService 类被定义为纯抽象类，由 3 个纯虚函数组成。
•addManager()：增加管理员
•deleteManager()：删除管理员
•listManager()：列出所有管理员

touch ManagerServiceImpl.cpp//实现类：ManagerServiceImpl

作为管理子系统业务逻辑层的实现类，ManagerServiceImpl 从纯抽象类 ManagerService 继承，并对基类中的 3 个纯虚函数提供覆盖版本。
•addManager()：增加管理员。将从参数传入的Manager对象加入managers容器
•deleteManager()：删除管理员。从managers容器中删除符合特定ID号的Manager对象
•listManager()：列出所有管理员。返回managers容器
•ManagerDao* dao：数据访问对象。构造函数中动态创建ManagerDaoFileImpl对象vector managers管理员对象容器。

4.1.3. 数据访问

touch ManagerDao.cpp//接口类：ManagerDao

作为管理子系统数据访问层的接口类，ManagerDao 类被定义为纯抽象类，由 2 个纯虚函数组成。
•load()：从数据存储读取管理员信息
•save()：将管理员信息写入数据存储

touch ManagerDaoFileImpl.cpp//实现类：ManagerDaoFileImpl

作为管理子系统数据访问层的实现类，ManagerDaoFileImpl 从纯抽象类 ManagerDao 继承，并对基类中的 2 个纯虚函数提供覆盖版本。
•load()：从文件读取管理员信息。以二进制方式整块读取Manager对象，加入从参数传入的管理员容器
•save()：将管理员信息写入文件。遍历从参数传入的管理员容器，以二进制方式整块写入每一个Manager对象

4.1.4. 逻辑对象

4.1.4.1. 管理员类：Manager
•int id：ID号
•char name[20]：用户名
•char password[20]：密码

===================================================================================================
===================================================================================================
===================================================================================================


touch ServiceView.cpp//接口类：ServiceView

作为业务子系统用户界面层的接口类，ServiceView 类被定义为纯抽象类，由 9 个纯虚函数组成。
•menu()：显示运营管理子菜单
•addDept()：处理增加部门菜单项
•deleteDept()：处理删除部门菜单项
•listDept()：处理列出部门菜单项
•addEmp()：处理增加员工菜单项
•deleteEmp()：处理删除员工菜单项
•modifyEmp()：处理修改员工信息菜单项
•listEmp()：处理列出部门员工菜单项
•listAllEmp()：处理列出部门员工菜单项

touch ServiceViewConsoleImpl.cpp//实现类：ServiceViewConsoleImpl

作为业务子系统用户界面层的实现类，ServiceViewConsoleImpl 从纯抽象类 ServiceView 继承，并对基类中的 9 个纯虚函数提供覆盖版本。
•menu()：通过控制台显示运营管理子菜单。在一个无限循环中不停显示菜单，接受用户选择，并根据用户所选菜单项调用其它八个接口函数。当用户选择返回时，终止循环，返回ManagerViewConsoleImpl的menu()函数
•addDept()：通过控制台处理增加部门菜单项。根据用户输入的部门名称创建Department对象，调用Service::addDept()接口函数增加部门
•deleteDept()：通过控制台处理删除部门菜单项。根据用户输入的部门ID号，调用Service::deleteDept()接口函数删除部门
•listDept()：通过控制台处理列出部门菜单项。调用Service::listDept()接口函数获得部门容器，遍历该容器并列表显示
•addEmp()：通过控制台处理增加员工菜单项。根据用户输入的员工姓名、性别和年龄创建Employee对象。再根据用户输入的部门ID号，调用Service::addEmp()接口函数增加员工。
•deleteEmp()：通过控制台处理删除员工菜单项。根据用户输入的员工ID号，调用Service::deleteEmp()接口函数删除部门
•modifyEmp()：通过控制台处理修改员工信息菜单项。根据用户输入的员工姓名、性别和年龄创建Employee对象，调用Service::modifyEmp()接口函数修改员工信息
•listEmp()：通过控制台处理列出部门员工菜单项。根据用户输入的部门ID号，调用Service::listEmp()接口函数列出部门员工
•listAllEmp()：通过控制台处理列出所有员工菜单项。调用
•Service::listAllEmp()：接口函数列出所有员工
•Service* m_pService：业务逻辑对象。构造函数中动态创建ServiceImpl对象

4.2.2. 业务逻辑

touch Service.cpp//接口类：Service

作为业务子系统业务逻辑层的接口类，Service 类被定义为纯抽象类，由 8 个纯虚函数组成。
•addDept()：增加部门
•deleteDept()：删除部门
•listDept()：列出部门
•addEmp()：增加员工
•deleteEmp()：删除员工
•modifyEmp()：修改员工信息
•listEmp()：列出部门员工
•listAllEmp()：列出所有员工

touch ServiceImpl.cpp//实现类：ServiceImpl

作为业务子系统业务逻辑层的实现类，ServiceImpl 从纯抽象类 Service 继承，并对基类中的 8 个纯虚函数提供覆盖版本。
•addDept()：增加部门。将从参数传入的Department对象加入m_vecDepts容器
•deleteDept()：删除部门。从m_vecDepts容器中删除符合特定ID号的Department对象
•listDept()：列出部门。返回m_vecDepts容器
•addEmp()：增加员工。根据部门ID号找到该员工所隶属的部门，将参数Employee对象加入该部门的m_vecEmps容器
•deleteEmp()：删除员工。依次调用每个部门的deleteEmp()接口函数
•modifyEmp()：修改员工信息。依次调用每个部门的modifyEmp()接口函数
•listEmp()：列出部门员工。根据部门ID号找到相应的部门，返回该部门的m_vecEmps容器
•listAllEmp()：列出所有员工。依次调用每个部门listEmp()接口函数，将处理结果汇总到一个容器中返回
•ServiceDao* m_pDao数据访问对象。构造函数中动态创建ServiceDaoFileImpl对象
•vector m_vecDepts：部门对象容器

4.2.3. 数据访问

touch ServiceDao.cpp//接口类：ServiceDao

作为业务子系统数据访问层的接口类，ServiceDao 类被定义为纯抽象类，由 2 个纯虚函数组成。
•load()：从数据存储读取部门及员工信息
•save()：将部门及员工信息写入数据存储

touch ServiceDaoFileImpl.cpp//实现类：ServiceDaoFileImpl

作为业务子系统数据访问层的实现类，ServiceDaoFileImpl 从纯抽象类 ServiceDao 继承，并对基类中的 2 个纯虚函数提供覆盖版本。
•load()：从文件读取部门及员工信息。以文本方式读取全部部门和员工信息，加入从参数传入的部门容器
•save()：将部门及员工信息写入文件。遍历从参数传入的部门容器，以文本方式写入每一个部门及其所有员工的信息


touch Department.cpp//部门类：Department
•deleteEmp()：删除本部门的员工。根据从参数传入的员工ID号，从m_vecEmps容器中删除相应员工，没找到则返回false
•listEmp()：列出本部门的员工。遍历m_vecEmps容器，将本部门的每位员工逐一加入从参数传入的员工容器
•modifyEmp()：修改本部门的员工信息。根据从参数传入的员工对象的ID号属性，从m_vecEmps容器中找到相应员工，更新其信息，没找到则返回false
•int m_nId：ID号
•string m_strName：名称
•vector m_vecEmps：员工对象容器

touch Employee//员工类：Employee

•int m_nId：ID号
•string m_strName：姓名
•bool m_bGender：性别，true表示男性，false表示女性
•int m_nAge：年龄























