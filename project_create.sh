touch managerview.h					#定义ManagerView抽象基类
touch managerview_console_impl.h	#声明ManagerViewConsoleImpl类
touch managerview_console_impl.cpp	#实现ManagerViewConsoleImpl类
touch managerservice.h				#定义ManagerService抽象基类
touch managerservice_impl.h			#声明ManagerServiceImpl类
touch managerservice_impl.cpp		#实现ManagerServiceImpl类
touch managerdao.h					#定义ManagerDao抽象基类
touch managerdao_file_impl.h		#声明ManagerDaoFileImpl类
touch managerdao_file_impl.cpp		#实现ManagerDaoFileImpl类
touch manager.h						#声明Manager类
touch manager.cpp					#实现Manager类

touch serviceview.h					#定义ServiceView抽象基类
touch serviceview_console_impl.h	#声明ServiceViewConsoleImpl类
touch serviceview_console_impl.cpp	#实现ServiceViewConsoleImpl类
touch service.h						#定义Service抽象基类
touch service_impl.h				#声明ServiceImpl类
touch service_impl.cpp				#实现ServiceImpl类
touch servicedao.h					#定义ServiceDao抽象基类
touch servicedao_file_impl.h		#声明ServiceDaoFileImpl类
touch servicedao_file_impl.cpp		#实现ServiceDaoFileImpl类
touch department.h					#声明Department类
touch department.cpp				#实现Department类
touch employee.h					#声明Employee类
touch employee.cpp					#实现Employee类

touch main.cpp						#定义main()函数
touch emis.h						#声明全局变量
touch emis.cpp						#定义全局变量
touch tools.h						#声明工具函数
touch tools.cpp						#定义工具函数

touch makefile						#项目制作脚本

