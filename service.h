#ifndef SERVICE_H
#define SERVICE_H
#include "department.h"

class Service
{
public:
	virtual bool addDept(Department dept)=0;
	virtual bool deleteDept(int deptid)=0;
	virtual void listDept(void)=0;
	virtual bool addEmp(int deptid,Employee emp)=0;
	virtual bool deleteEmp(int deptid,int empid)=0;
	virtual bool modifyEmp(int deptid,int empid)=0;
	virtual bool listEmp(int deptid)=0;
	virtual void listAllEmp(void)=0;
};
#endif//SERVICE_H
