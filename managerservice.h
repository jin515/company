#ifndef MANAGERSERVICE_H
#define MANAGERSERVICE_H
#include "emis.h"
class ManagerService
{
public:
	virtual bool addManager(Manager manager)=0;
	virtual bool deleteManager(int id)=0;
	virtual void listManager(void)=0;
};
#endif//MANAGERSERVICE_H
