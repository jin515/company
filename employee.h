#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include <string>
using namespace std;

class Employee
{
private:
    int m_nId;           //ID号
    string m_strName;    //姓名
    bool m_bGender;      //性别，true表示男性，false表示女性
    int m_nAge;          //年龄
public:
	Employee(int id=0,string name="",bool gender=true,int age=0)
	{
		m_nId=id;
		m_strName=name;
		m_bGender=gender;
    	m_nAge=age;
	}

    int get_m_nId(void);
    string get_m_strName(void);
    bool get_m_bGender(void);
    int get_m_nAge(void);
	
	//void show_emp(void);
    
    void set_m_nId(int id);
    void set_m_strName(string name);
    void set_m_bGender(bool gender);
    void set_m_nAge(int age);
};

#endif //EMPLOYEE_H
