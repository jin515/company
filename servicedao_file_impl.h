#ifndef SERVICEDAO_FILE_IMPL_H
#define SERVICEDAO_FILE_IMPL_H
#include "servicedao.h"
class ServiceDao_impl:public ServiceDao
{
public:
    void load(vector<Department>& data);
	void save(vector<Department>& data);
};
#endif //SERVICEDAO_FILE_IMPL_H
