#include "managerdao.h"
class ManagerDaoFileImpl:public ManagerDao
{
public:
    void load(vector<Manager>& managers);
    void save(vector<Manager>& managers);

};
