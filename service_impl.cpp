#include "service_impl.h"
#include "emis.h" 
bool Service_impl::addDept(Department dept)
{
    m_vecDepts.push_back(dept);
    return true;
}
bool Service_impl::deleteDept(int deptid)
{
    for(int i=0;i<m_vecDepts.size();i++)
    {
        if(deptid == m_vecDepts[i].get_d_nId())
        {
            m_vecDepts.erase(m_vecDepts.begin()+i);
            return true;
        }
    }return false;
}

void Service_impl::listDept(void)
{
    for(int i=0;i<m_vecDepts.size();i++)
    {
        cout<<"Department ID:"<<m_vecDepts[i].get_d_nId()<<" ";
        cout<<"Name:"<<m_vecDepts[i].get_d_strName()<<" ";
        cout<<"number:"<<m_vecDepts[i].get_d_number()<<endl;
    }
}

bool Service_impl::addEmp(int deptid,Employee emp)
{
    for(int i=0;i<m_vecDepts.size();i++)
    {
        if(deptid==m_vecDepts[i].get_d_nId())
        {
            m_vecDepts[i].add_emp(emp);
            return true;
        }
    }
    return false;
}

bool Service_impl::deleteEmp(int deptid,int empid)
{
    for(int i=0;i<m_vecDepts.size();i++)
    {
        if(deptid==m_vecDepts[i].get_d_nId())
        {
            m_vecDepts[i].deleteEmp(empid);
            return true;
        }
    }
    return false;
}

bool Service_impl::modifyEmp(int deptid,int empid)
{
    for(int i=0;i<m_vecDepts.size();i++)
    {
        if(deptid==m_vecDepts[i].get_d_nId())
        {
            m_vecDepts[i].modifyEmp(empid);
            return true;
        }
    }
    return false;
}

bool Service_impl::listEmp(int deptid)
{
    for(int i=0;i<m_vecDepts.size();i++)
    {
        if(deptid == m_vecDepts[i].get_d_nId())
        {
            m_vecDepts[i].listEmp();
            return true;
        }
    }
    return false;
   
}

void Service_impl::listAllEmp(void)
{
    for(int i=0;i<m_vecDepts.size();i++)
    {
        m_vecDepts[i].listEmp();
        cout<<"----------------------------------------"<<endl;
    }
}