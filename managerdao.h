#ifndef MANAGERDAO_H
#define MANAGERDAO_H
#include <vector>
#include "manager.h"
using namespace std;
class ManagerDao
{
public:
    virtual void load(vector<Manager>& managers)=0;
    virtual void save(vector<Manager>& managers)=0;
};
#endif//MANAGERDAO_H
