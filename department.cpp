#include <iostream>
#include "department.h"
using namespace std;
int Department::get_d_nId(void)
{
	return d_nId;
}

string Department::get_d_strName(void)
{
	return d_strName;
}

int Department::get_d_number(void)
{
	return d_number;
}

void Department::set_d_nId(int id)
{
	d_nId=id;
}

void Department::set_d_strName(string name)
{
	d_strName=name;
}
Employee& Department::get_index_emp(int index)
{
	return m_vecEmps[index];
}

bool Department::add_emp(Employee& emp)
{
	m_vecEmps.push_back(emp);
	d_number++;
	return true;
}

bool Department::deleteEmp(int id)
{
	
	for(int i=0;i<m_vecEmps.size();i++)
	{
		if(id==m_vecEmps[i].get_m_nId())
		{
			m_vecEmps.erase(m_vecEmps.begin()+i-1);//================================
			d_number--;
			return true;
		}
	}return false;
}

void Department::listEmp(void)
{
	for(int i=0;i<m_vecEmps.size();i++)
	{
		cout<<"ID:"<<m_vecEmps[i].get_m_nId()<<" ";
		cout<<"Name:"<<m_vecEmps[i].get_m_strName()<<" ";
		if(m_vecEmps[i].get_m_bGender())
		{
			cout<<"Gender:male    ";
		}else cout<<"Gender:female  ";
		cout<<"Age:"<<m_vecEmps[i].get_m_nAge()<<endl;
	}
}

bool Department::modifyEmp(int id)
{
	cout<<"请输入新姓名：";
	string name;
	cin>>name;
	cout<<"请输入新性别(1=male，0=female)：";
	int gender;
	cin>>gender;
	cout<<"请输入新年龄：";
	int age;
	cin>>age;
	for(int i=0;i<m_vecEmps.size();i++)
	{
		if(id==m_vecEmps[i].get_m_nId())
		{
			m_vecEmps[i].set_m_strName(name);
			m_vecEmps[i].set_m_bGender(gender);
			m_vecEmps[i].set_m_nAge(age);
			return true;
		}
	}
	return false;
	
}
/*
int main()
{
	Employee emp1(1001,"absd",1,18);
	Employee emp2(1002,"qqqw",0,25);
	Employee emp3(1003,"erwe",1,33);
	Employee emp4(1004,"tewe",0,87);
	Department d1(909,"gamepart");
	
	d1.add_emp(emp1);
	d1.add_emp(emp2);
	d1.add_emp(emp3);
	d1.add_emp(emp4);
	
	d1.listEmp();
	cout<<"+================================"<<endl;
	d1.deleteEmp(1002);
	d1.listEmp();
	d1.modifyEmp(1001);
	d1.listEmp();
}*/
