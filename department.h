#ifndef DEPARTMENT_H
#define DEPARTMENT_H
#include <vector>
#include <string>
#include "employee.h"
using namespace std;
class Department
{
private:
	int d_nId;//ID号
	int d_number;//部门人数
	string d_strName;//名称
	vector<Employee> m_vecEmps;//员工对象容器

public:
	Department(int id,string name)
	{
		vector<Employee> m_vecEmps;
		d_number=0;
		d_nId=id;
		d_strName=name;
	}
	Department(int id,string name,int number)
	{
		vector<Employee> m_vecEmps;
		d_number=number;
		d_nId=id;
		d_strName=name;
	}
	int get_d_nId(void);
	string get_d_strName(void);
	int get_d_number(void);

	void set_d_nId(int id);
	void set_d_strName(string name);
	
	Employee& get_index_emp(int index);
	bool add_emp(Employee& emp);
	bool deleteEmp(int id);//删除本部门的员工。根据从参数传入的员工ID号，从m_vecEmps容器中删除相应员工，没找到则返回false
	void listEmp(void);//列出本部门的员工。遍历m_vecEmps容器，将本部门的每位员工逐一加入从参数传入的员工容器
	bool modifyEmp(int id);//修改本部门的员工信息。
};
#endif	//DEPARTMENT_H
