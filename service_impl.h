#ifndef SERVICE_IMPL_H
#define SERVICE_IMPL_H
#include "service.h"

class Service_impl:public Service
{
public:
	bool addDept(Department dept);
	bool deleteDept(int deptid);
	void listDept(void);
	bool addEmp(int deptid,Employee emp);
	bool deleteEmp(int deptid,int empid);
	bool modifyEmp(int deptid,int empid);
	bool listEmp(int deptid);
    void listAllEmp(void);
};
#endif //SERVICE_IMPL_H