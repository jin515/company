#ifndef MANAGER_H
#define MANAGER_H
#include <iostream>
#include <string.h>
using namespace std;
class Manager
{
private:
    int id;
    char name[20];
    char password[20];
public:
    Manager(int id=0,char* name="null",char* password="null")
    {
        this->id=id;
        strcpy(this->name,name);
        strcpy(this->password,password);
    }
    void view_manager(void);
    int get_id(void);
    string get_name(void);
    string get_password(void);
};

#endif //MANAGER_H
