#ifndef LOGIN_H
#define LOGIN_H

class Login
{
public:
    virtual void login(void)=0;
    virtual void loginsup(void)=0;
};

#endif  //LOGIN_H