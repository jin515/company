#ifndef SERVICEDAO_H
#define SERVICEDAO_H
#include "emis.h" 
class ServiceDao
{
public:
	virtual void load(vector<Department>& data)=0;
	virtual void save(vector<Department>& data)=0;
};
#endif//SERVICEDAO_H
